package com.patterns.strategy.character;

public abstract class Character {

  private WeaponBehavior weaponBehavior;

  public void setWeaponBehavior(WeaponBehavior weaponBehavior) {
    this.weaponBehavior = weaponBehavior;
  }

  public void fight() {
    this.weaponBehavior.useWeapon();
  }
}

