package com.patterns.strategy.character;

public interface WeaponBehavior {

  void useWeapon();
}
