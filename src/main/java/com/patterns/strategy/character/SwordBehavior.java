package com.patterns.strategy.character;

public class SwordBehavior implements WeaponBehavior {

  public void useWeapon() {
    System.out.println("SwordBehavior!");
  }
}
