package com.patterns.strategy.character;

public class CharacterMain {
  public static void main(String[] args) {

    Character character = new King();

    character.setWeaponBehavior(new SwordBehavior());
    character.fight();

    character.setWeaponBehavior(new KnifeBehavior());
    character.fight();

  }
}
