package com.patterns.strategy.character;

public class AxeBehavior implements WeaponBehavior {

  public void useWeapon() {
    System.out.println("AxeBehavior!");
  }
}
