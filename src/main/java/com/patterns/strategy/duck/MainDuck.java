package com.patterns.strategy.duck;

public class MainDuck {
  public static void main(String[] args) {

    Duck duck = new MallardDuck();
    duck.performFly();
    duck.performQuack();
    System.out.println("-------------------");
    duck.setFlyBehavior(new FlyWithWings());
    duck.setQuackBehavior(new Quack());

    duck.performQuack();
    duck.performFly();
    duck.display();
    duck.swim();
  }
}
