package com.patterns.strategy.duck;

public class MuteQuack implements QuackBehavior {

  public void quack() {
    System.out.println("MuteQuack!");
  }
}
