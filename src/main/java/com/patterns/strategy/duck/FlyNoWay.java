package com.patterns.strategy.duck;

public class FlyNoWay implements FlyBehavior {

  public void fly() {
    System.out.println("FlyNoWay!");
  }
}
