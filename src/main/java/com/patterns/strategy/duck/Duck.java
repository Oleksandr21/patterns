package com.patterns.strategy.duck;

public abstract class Duck {

  private QuackBehavior quackBehavior = new Quack();
  private FlyBehavior flyBehavior = new FlyNoWay();

  public void setQuackBehavior(QuackBehavior quackBehavior) {
    this.quackBehavior = quackBehavior;
  }

  public void setFlyBehavior(FlyBehavior flyBehavior) {
    this.flyBehavior = flyBehavior;
  }

  public abstract void display();

  public void performQuack() {
    this.quackBehavior.quack();
  }

  public void performFly() {
    this.flyBehavior.fly();
  }
  public void swim() {
    System.out.println("All ducks float, even decoys!");
  }

}
