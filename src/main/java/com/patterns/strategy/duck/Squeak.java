package com.patterns.strategy.duck;

import com.patterns.strategy.duck.QuackBehavior;

public class Squeak implements QuackBehavior {

  public void quack() {
    System.out.println("Squeak!");
  }
}
