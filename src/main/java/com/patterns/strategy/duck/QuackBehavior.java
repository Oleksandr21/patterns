package com.patterns.strategy.duck;

public interface QuackBehavior {

  void quack();
}
