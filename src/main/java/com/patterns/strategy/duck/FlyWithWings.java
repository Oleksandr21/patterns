package com.patterns.strategy.duck;

public class FlyWithWings implements FlyBehavior {

  public void fly() {
    System.out.println("FlyWithWings!");
  }
}
