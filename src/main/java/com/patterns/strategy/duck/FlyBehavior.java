package com.patterns.strategy.duck;

public interface FlyBehavior {

  void fly();
}
